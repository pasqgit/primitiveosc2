import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export default Objectes = new Mongo.Collection('objectes');

Meteor.methods({
  'objectes.insert'(
    tipus,
    nom,
    valor,
    posicio
  ){
    return Objectes.insert({
      tipus,
      nom, 
      valor,
      posicio,
      createdAt: new Date()
    });
  },

  'objectes.remove'(objecte){
      Objectes.remove(objecte._id);
  },

  'objectes.nom.update'(
    idObjecte,
    nomVal
  ){
      return Objectes.update(idObjecte, {
        $set: {
          nom: nomVal
        }
      });
  },

  'objectes.valor.update'(
    idObjecte,
    valor
  ){
      return Objectes.update(idObjecte, {
        $set: {
          valor
        }
      });
  },

  'objectes.posicio.update'(
    idObjecte,
    posicio
  ){
      return Objectes.update(idObjecte, {
        $set: {
          posicio
        }
      });
  }

});
  
