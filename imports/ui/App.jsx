import { Meteor } from 'meteor/meteor';
import React, { useState, useEffect } from 'react';
//import Hello from './Hello.jsx';
//import Info from './Info.jsx';
import Objectes from '/imports/api/objectes.js';
import { withTracker } from 'meteor/react-meteor-data';
import CheckBox from './CheckBox.jsx';
import Slider from './Slider.jsx';
//import { UDPPort } from 'osc';

const App = (props) => {
  let 
    numObjectes = 0,
    tVal= 100, 
    lVal = 50
  ;

  //   udpPort = new UDPPort({
  //     remoteAddress: "192.168.43.166",
  //     remotePort: 57121,
  //     metadata: true
  //   })

  // ;

  // // Open the socket.
  // udpPort.open();
  
  
  // When the port is read, send an OSC message to, say, SuperCollider
  // udpPort.on("ready", function () {
  //     udpPort.send({
  //         address: "/s_new",
  //         args: [
  //             {
  //                 type: "s",
  //                 value: "default"
  //             },
  //             {
  //                 type: "i",
  //                 value: 100
  //             }
  //         ]
  //     }, "192.168.43.166", 57110);
  // });

  //useEffect(()=>{}, [props.objectes]);

  const LlistaCheckBoxes = props => <>
    {props.objectes.filter((v,i,a) => v.tipus == "checkbox").map((v,i,a) => <CheckBox key={i} nom={v.nom} marcat={v.valor} idObjecte={v._id} />)}
  </>;

  const LlistaSliderHs = props => <>
    {props.objectes.filter((v,i,a) => v.tipus == "sliderH").map((v,i,a) => <Slider key={i} nom={v.nom} valor={v.valor} idObjecte={v._id} />)}
  </>;

  return (
    <div>
      <h1>CheckBox en Meteor!</h1>
      <button
        style={{
          display: `block`
        }}
        onClick={() => {
          Meteor.call(
            'objectes.insert',
            `checkbox`,
            `obj_${numObjectes}`,
            false,
            {top: tVal, left: lVal},
            (err, res) => {
              //tVal = tVal + <increment_vertical>
              lVal += 20;
              numObjectes += 1;
            }
          );}
        }
      >
        Afegeix CheckBox
      </button>
      {/* <CheckBox 
        nom="LoCheckBox"
      /> */}

      <LlistaCheckBoxes {...props} />

      <hr />
      <button
        style={{
          display: `block`
        }}
        onClick={() => {
          Meteor.call(
            'objectes.insert',
            `sliderH`,
            `obj_${numObjectes}`,
            false,
            {top: tVal, left: lVal},
            (err, res) => {
              //tVal = tVal + <increment_vertical>
              lVal += 20;
              numObjectes += 1;
            }
          );}
        }
      >
        Afegeix Slider
      </button>
      
        <LlistaSliderHs {...props} />


    </div>
  );
};



const AppAmbDades = withTracker(() => {
  const 
    subscriptObjectes = Meteor.subscribe('objectes'),
    loading = !subscriptObjectes.ready(),
    objectes = !loading ? Objectes.find().fetch() : []
  ;
  
  return { loading, objectes };
})(App);

export default AppAmbDades;