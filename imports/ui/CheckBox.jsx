import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';
// import { withTracker } from 'meteor/react-meteor-data';
// import Objectes from '../api/objectes';
//import osc from 'osc';
//import { Client, Server } from 'node-osc';
//import Messenger from 'udp-messenger';


export default CheckBox = props => {
  const 
    [marcat, setMarcat] = useState(props.marcat)
 // ,
    // udp = new osc.UDPPort({
    //   localAddress: "127.0.0.1", // shouldn't matter here
    //   localPort: 5000, // not receiving, but here's a port anyway
    //   remoteAddress: "192.168.0.37", // the other laptop
    //   remotePort: 3333 // the port to send to
    // })
  ;

//  udp.open();
//
//  udp.on("ready", function () {
//
//      console.log("ready");
//      setInterval(function () {
//          udp.send({
//              address: "/sending/every/second",
//              args: [1, 2, 3]
//          })
//      }, 1000);
//});
  // ,
  // //   messenger = new Messenger(2000, 54000, 512)
  //   client = new Client('192.168.0.37', 3333);
 // ;

  // Open the socket.
  // udpPort.open();

  // // Every second, send an OSC message to SuperCollider
  // //setInterval(function() {
  //     var msg = {
  //         address: "/hello/from/oscjs",
  //         args: [
  //             {
  //                 type: "f",
  //                 value: Math.random()
  //             },
  //             {
  //                 type: "f",
  //                 value: Math.random()
  //             }
  //         ]
  //     };

  //     console.log("Sending message", msg.address, msg.args, "to", udpPort.options.remoteAddress + ":" + udpPort.options.remotePort);
  //     udpPort.send(msg);
  // //}, 1000);

  // osc.open();

  return (
    <input
      type="checkbox"
      nom={props.nom}
      checked={marcat}
      onChange={() => {

        // client.send('/hello', 'world', (err) => {
        //   if (err) console.error(err);
        //   client.close();
        // });
        //messenger.send(new Buffer("Hello World!") , '192.168.1.3', 52000);

        setMarcat(!marcat);
        Meteor.call(
          'objectes.valor.update',
          props.idObjecte,
          !marcat
          // ,
          // (err, res) => setMarcat(props.objectes.find(o => o._id == props.idObjecte).valor)
        );
      }}
    />
  );
};

// export default CheckBoxContainer = withTracker(() => {
//   return {
//     objectes: Objectes.find().fetch()
//   };
// })(CheckBox);