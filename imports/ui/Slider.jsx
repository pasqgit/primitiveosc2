import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';
//import ReactSlider from 'react-slider';
// import { withTracker } from 'meteor/react-meteor-data';
// import Objectes from '../api/objectes';


export default Slider = props => {
  const 
    [valor, setValor] = useState(props.valor)
  ;

  return (
    <input 
      type="range" 
      min="1" 
      max="100" 
     // value={valor}
      value={valor}
      className="slider"
      // onChange={(v) => {
      //   setValor(v);
      // }}
      onInput={(ev) => {

        // client.send('/hello', 'world', (err) => {
        //   if (err) console.error(err);
        //   client.close();
        // });
        //messenger.send(new Buffer("Hello World!") , '192.168.1.3', 52000);

        setValor(ev.target.value);
        Meteor.call(
          'objectes.valor.update',
          props.idObjecte,
          ev.target.value
          // ,
          // (err, res) => setMarcat(props.objectes.find(o => o._id == props.idObjecte).valor)
        );
      }}

      onChange={(ev) => {

        // client.send('/hello', 'world', (err) => {
        //   if (err) console.error(err);
        //   client.close();
        // });
        //messenger.send(new Buffer("Hello World!") , '192.168.1.3', 52000);

        setValor(ev.target.value);
        Meteor.call(
          'objectes.valor.update',
          props.idObjecte,
          ev.target.value
          // ,
          // (err, res) => setMarcat(props.objectes.find(o => o._id == props.idObjecte).valor)
        );
      }}
    />
  );
};

// export default CheckBoxContainer = withTracker(() => {
//   return {
//     objectes: Objectes.find().fetch()
//   };
// })(CheckBox);