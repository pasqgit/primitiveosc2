import Objectes from '/imports/api/objectes.js';


// Publish Objectes
Meteor.publish('objectes', function(){
    return Objectes.find();
});